from bottle import route, template


@route('/v1/hello/<name>', method="GET")
def hello(name):
    return template(
        '<b>Hello {{name.title() if name else "stranger"}}</b>!',
        name=name
    )


@route('/v2/hello/<name:re:.*>', "GET")
def hello2(name=''):
    return template(
        '<b>Hello {{name.title() if name else "stranger"}}</b>!',
        name=name
    )
