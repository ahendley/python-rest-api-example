from bottle import Bottle
try:
    import api
except ImportError:
    from app import api


web_app = Bottle()

web_app.route('/v1/hello/<name>', "GET", api.hello)
web_app.route('/v2/hello/<name:re:.*>', "GET", api.hello2)
